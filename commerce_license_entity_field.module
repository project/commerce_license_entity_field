<?php

/**
 * @file
 * Contains hook implementations for Commerce License Entity Field.
 */

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter().
 */
function commerce_license_entity_field_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Remove the delete action and show a message on the form for an entity that
  // has a field value controlled by a license.
  // We don't allow deleting the entity because it would then put the license
  // in an illogical state, and if the license is tied to a subscription, would
  // cause the owner to be charged for something they no longer have.
  $form_object = $form_state->getFormObject();
  if (!($form_object instanceof EntityFormInterface)) {
    // We're only interested in forms for an entity.
    return;
  }

  /** @var EntityFormInterface $form_object */
  $entity = $form_object->getEntity();
  $entity_type_id = $entity->getEntityTypeId();

  // Don't bother doing anything if the user can't delete the entity anyway.
  if (!$entity->access('delete')) {
    return;
  }

  // Get all active licenses that affect this entity.
  $license_storage = \Drupal::service('entity_type.manager')->getStorage('commerce_license');
  $query = $license_storage->getQuery()
    ->accessCheck(FALSE)
    ->condition('type', 'entity_field')
    ->condition('state', 'active')
    ->condition('license_target_entity.target_type', $entity_type_id)
    ->condition('license_target_entity.target_id', $entity->id());
  $license_ids = $query->execute();

  if (empty($license_ids)) {
    return;
  }

  $licenses = $license_storage->loadMultiple($license_ids);

  foreach ($licenses as $license) {
    // Block access to the delete action.
    // TODO: implement hook_entity_access() to do this at the API level too.
    $form['actions']['delete']['#access'] = FALSE;

    $message = t("The @license-label license is controlling the @field-label value. This @entity-label may not be deleted while the license is active. ", [
      '@license-label' => $license->label(),
      '@field-label' => $entity->getFieldDefinition($license->license_target_field->value)->getLabel(),
      '@entity-label' => \Drupal::service('entity_type.manager')->getDefinition($entity_type_id)->getSingularLabel(),
    ]);

    // If the field is displayed in the form, also explain that it's locked.
    if (!empty($form[$license->license_target_field->value])) {
      $message .= ' ' . t("The field value may not be changed.");
    }

    // TODO: handle licenses set to renew automatically.
    // TODO: handle unlimited licenses.
    if (!empty($license->getExpiresTime())) {
      $message .= ' ' . t("The license expires on @expiry.", [
        '@expiry' => \Drupal::service('date.formatter')->format($license->getExpiresTime(), 'long'),
      ]);
    }

    \Drupal::messenger()->addStatus($message);
  }
}
